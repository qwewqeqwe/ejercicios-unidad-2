﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crud
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        BaseDeDatos bd = new BaseDeDatos();

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bd.SelectDataTable("select * from articulos");

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            String agregar = "insert into articulos values (" + textBox1.Text + ", '" + textBox2.Text + "', " + textBox3.Text + ")";
            if (bd.executecommand(agregar)){
                MessageBox.Show("Exito");
                dataGridView1.DataSource = bd.SelectDataTable("select * from articulos");
            }
            else
            {
                MessageBox.Show("Error");
            }
            
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            string actualizar = "update articulos set precio=" + textBox3.Text + "," + "descripcion='" + textBox2.Text + "' where codigo=" + textBox1.Text;
            if (bd.executecommand(actualizar))
            {
                MessageBox.Show("Exito");
                dataGridView1.DataSource = bd.SelectDataTable("select * from articulos");
            }
            else
            {
                MessageBox.Show("Error");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            string eliminar = "delete from articulos where codigo=" + textBox1.Text;
            if (bd.executecommand(eliminar))
            {
                MessageBox.Show("Exito");
                dataGridView1.DataSource = bd.SelectDataTable("select * from articulos");
            }
            else
            {
                MessageBox.Show("Error");
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string buscarPorId = "select * from articulos where codigo=" + textBox1.Text;
            dataGridView1.DataSource = bd.SelectDataTable(buscarPorId);
        }
    }
}
